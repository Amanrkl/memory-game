const gameContainer = document.getElementById("game");
const gameScreen = document.querySelector(".game-screen");
const moves = document.querySelector(".moves");
const timer = document.querySelector(".timer");
const start = document.querySelector(".start");
const win = document.querySelector(".win");
const rules = document.querySelector('.rules');
const scoreScreen = document.querySelector('.bestScore');
const level = document.querySelector('.level');
const rulesBtn = document.querySelector('.rules-btn');

const state = {
  gameStarted: false,
  NoOfCardsFlipped: 0,
  totalTime: 0,
  totalMoves: 0,
  loop: null,
  level: 2,
};

const gifsArray = new Array(12)
  .fill(0)
  .map((gif, index) => {
    return {
      id: index + 1,
      gifSrc: `./gifs/${index + 1}.gif`
    }
  })

setLevel(state.level);
setBestScore();

document.querySelector('.main').addEventListener('click', (event) => {

  if (event.target.classList.contains('start')) {
    if (!state.gameStarted) {
      startGame();
      gameScreen.classList.remove('inactive');
      level.classList.add('inactive');
      scoreScreen.classList.add('inactive');
    } else {
      resetGame();
      gameScreen.classList.add('inactive');
      level.classList.remove('inactive');
      scoreScreen.classList.remove('inactive');
    }
    win.classList.add('inactive');
    rulesBtn.classList.remove('inactive');
  }

  if (event.target.classList.contains('rules-btn')) {
    rules.classList.remove('inactive');
    gameScreen.classList.add('inactive');
    win.classList.add('inactive');
  }

  if (event.target.classList.contains('close')) {
    rules.classList.add('inactive');
    if (state.gameStarted) {
      gameScreen.classList.remove('inactive');
      scoreScreen.classList.add('inactive');
      level.classList.add('inactive');
    }
  }

  if (event.target.classList.contains('up') && state.level < 7) {
    state.level++;
    resetGame();
    document.querySelector('.currLevel span').textContent = state.level;
  }

  if (event.target.classList.contains('down') && state.level > 1) {
    state.level--;
    resetGame();
    document.querySelector('.currLevel span').textContent = state.level;
  }

})

function setLevel(dimensions) {
  let picks = pickRandom(gifsArray, dimensions * 2);
  let items = shuffle([...picks, ...picks]);
  createDivsForCards(items);
}

function shuffle(array) {
  let counter = array.length;

  while (counter > 0) {
    let index = Math.floor(Math.random() * counter);

    counter--;

    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

function pickRandom(array, NoOfPatterns) {
  const clonedArray = [...array];
  const randomPicks = [];

  for (let index = 0; index < NoOfPatterns; index++) {
    const randomIndex = Math.floor(Math.random() * clonedArray.length);

    randomPicks.push(clonedArray[randomIndex]);
  }

  return randomPicks;
};

function createDivsForCards(cardsArray) {
  gameContainer.textContent = "";
  for (let currCard of cardsArray) {
    const newDiv = document.createElement("div");
    const frontDiv = document.createElement("div");
    const backDiv = document.createElement("div");

    newDiv.classList.add("card");
    frontDiv.classList.add("card-front");
    backDiv.classList.add("card-back");

    backDiv.style.backgroundImage = `url(${currCard.gifSrc})`;
    newDiv.dataset.id = currCard.id;

    newDiv.addEventListener("click", handleCardClick);

    newDiv.appendChild(frontDiv);
    newDiv.appendChild(backDiv);
    gameContainer.append(newDiv);
  }
}

function handleCardClick(event) {

  const eventParent = event.target.parentElement;

  if (eventParent.classList.contains('card') && !eventParent.classList.contains('flipped')) {
    state.NoOfCardsFlipped++;
    flipCard(eventParent);
  }

}

function startGame() {
  state.gameStarted = true;
  console.log("hello")

  if (!state.loop) {
    state.loop = setInterval(() => {
      state.totalTime++;

      moves.textContent = `${state.totalMoves} moves`;
      timer.textContent = `time: ${state.totalTime} sec`;
    }, 1000);
  }
};

function resetGame() {
  state.gameStarted = false;
  state.NoOfCardsFlipped = 0;
  state.totalTime = 0;
  state.totalMoves = 0;
  clearInterval(state.loop);
  state.loop = null;

  moves.textContent = `${state.totalMoves} moves`;
  timer.textContent = `time: ${state.totalTime} sec`;

  setLevel(state.level);
  setBestScore();
}

function setBestScore() {
  const currBestScore = localStorage.getItem(state.level);

  if (currBestScore) {
    updateBestScore(currBestScore)
  } else {
    document.querySelector('.bestScoreValue').textContent = 'Nil';
  }
}

function updateBestScore(currScore) {
  const bestScoreElement = document.querySelector('.bestScoreValue');
  if (bestScoreElement.textContent === 'Nil') {
    localStorage.setItem(state.level, currScore);
    bestScoreElement.textContent = currScore;
  } else {
    let currBestScore = localStorage.getItem(state.level);
    if (Number(currBestScore) >= Number(currScore) && Number(currBestScore) > 0) {
      localStorage.setItem(state.level, currScore);
      bestScoreElement.textContent = currScore;
    }
  }
}

function flipCard(card) {

  if (!state.gameStarted) {
    startGame();
  }

  if (state.NoOfCardsFlipped <= 2) {
    card.classList.add("flipped");
    state.totalMoves++;
  }

  if (state.NoOfCardsFlipped === 2) {
    const flippedCards = document.querySelectorAll(".flipped:not(.matched)");
    if (flippedCards[0].dataset.id === flippedCards[1].dataset.id) {
      flippedCards[0].classList.add("matched");
      flippedCards[1].classList.add("matched");
    }

    console.log(flippedCards)
    setTimeout(() => {
      flipBackCards();
    }, 1000);
  }

  if (!document.querySelectorAll(".card:not(.flipped)").length) {
    console.log("Player Won")
    updateBestScore(state.totalMoves);
    gameScreen.classList.add('inactive');
    scoreScreen.classList.remove('inactive');
    rulesBtn.classList.add('inactive');
    win.classList.remove('inactive');
    clearInterval(state.loop);
  }
};

function flipBackCards() {
  document.querySelectorAll(".card:not(.matched)").forEach((card) => {
    card.classList.remove("flipped");
  });
  state.NoOfCardsFlipped = 0;
};